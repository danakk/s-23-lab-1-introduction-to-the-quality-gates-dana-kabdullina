FROM maven:3.6.0-jdk-11-slim AS build
WORKDIR /app
COPY src /app/src
COPY pom.xml /app/pom.xml
RUN mvn clean package

FROM openjdk:11-jre-slim
COPY --from=build /app/target/main-0.0.1-SNAPSHOT.jar /app/main.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/main.jar"]